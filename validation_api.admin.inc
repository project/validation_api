<?php

/**
 * @file
 */

/**
 * Admin Page
 */
function validation_api_admin() {
  $content = array(
    array(
      'title' => t('Validators'),
      'href' => 'admin/build/validation_api/validators',
      'localized_options' => array('attributes' => array('title' => t('Create and manage validator rules, messages, etc.'))),
      'description' => t('Create and manage validator rules, messages, etc.'),
    ),
    array(
      'title' => t('Fields'),
      'href' => 'admin/build/validation_api/fields',
      'localized_options' => array('attributes' => array('title' => t('Create and manage a form field\'s validator, argument, etc.'))),
      'description' => t('Create and manage a form field\'s validator, argument, etc.'),
    ),
    array(
      'title' => t('Settings'),
      'href' => 'admin/build/validation_api/settings',
      'localized_options' => array('attributes' => array('title' => t('Modify settings for the Validation API system.'))),
      'description' => t('Modify settings for the Validation API system.'),
    )
  );

  return theme('admin_block_content', $content);
}

/**
 * Settings Form
 */
function validation_api_admin_settings_form() {
  $form['ajax_submission'] = array(
    '#type' => 'checkbox',
    '#title' => t('AJAX Submission on all forms'),
    '#description' => t('This will validate all forms when submitting via AJAX before allowing the submit to continue.'),
    '#default_value' => variable_get('ajax_submission', 1),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings'),
  );

  return $form;
}

/**
 * Submit for Settings Form
 */
function validation_api_admin_settings_form_submit($form_id, &$form_state) {
  variable_set('ajax_submission', $form_state['values']['ajax_submission']);

  drupal_set_message(t('AJAX Submission is %s.', array('%s' => (($form_state['values']['ajax_submission'] == 1) ? t('enabled') : t('disabled')))));
}

/**
 * Lists all validators for the Admin Page
 */
function validation_api_admin_validators() {
  $validators = _validation_api_validators();

  if (isset($validators) && is_array($validators) && count($validators) > 0) {
    $header = array(t('Name'), t('Type'), t('Message'), t('Ops'));
    $rows = array();
    $hook_validators = array();
    $update_str = '';
    foreach ($validators as $key => $validator) {
      $update_str = '';
      if (!empty($validator->module)) {
        if (!isset($hook_validators[$validator->module])) {
          $hook_validators[$validator->module] = module_invoke($validator->module, HOOK_ADD_VALIDATOR);
        }
        $fields = array('type', 'rule', 'message');
        $code = _validation_api_export(array($validator), $fields);
        $hook_code = '';
        foreach ($fields as $field) {
          $hook_code .= '$validators[0]->'. $field .' = \''. $hook_validators[$validator->module][$validator->delta]->$field ."';\n";
        }
        $hook_code .= 'return $validators;';
        $update_str = ($code != $hook_code) ? ' '. l(t('update'), 'admin/build/validation_api/validators/update/'. $validator->vavid) : '';
      }
      $rows[] = array($validator->name, $validator->type, $validator->message, l(t('edit'), 'admin/build/validation_api/validators/edit/'. $validator->vavid) .' '. l(t('delete'), 'admin/build/validation_api/validators/delete/'. $validator->vavid) . $update_str);
    }

    $output = theme('table', $header, $rows);
  }
  else {
    $output = t('There are no validators in the database. !add or !import a validator.', array('!add' => l(t('Add'), 'admin/build/validation_api/validators/add'), '!import' => l(t('Import'), 'admin/build/validation_api/validators/import')));
  }

  return $output;
}

/**
 * The form for Adding or Editing Validators
 */
function validation_api_admin_validators_form($form_state, $vavid = NULL) {
  $defaults = array('name' => '', 'type' => '', 'rule' => '', 'message' => '');
  if (!is_null($vavid)) {
    drupal_set_breadcrumb(array_merge(drupal_get_breadcrumb(), array(l(t('Validators'), 'admin/build/validation_api/validators'))));
    if ($array = db_fetch_array(db_query('SELECT * FROM {validation_api_validators} WHERE vavid = %d', $vavid))) {
      $defaults = $array;
    }
    else {
      drupal_not_found();
      exit;
    }
  }

  if (isset($defaults['vavid'])) {
    $form['#redirect'] = 'admin/build/validation_api/validators';
    $form['vavid'] = array(
      '#type' => 'value',
      '#value' => $defaults['vavid'],
    );
  }
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('A unique name for this validator.'),
    '#required' => TRUE,
    '#maxlength' => 32,
    '#default_value' => $defaults['name'],
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#description' => t('The language to run your code in the rule.'),
    '#options' => array('php' => 'PHP', 'regex' => 'Regular Expression'),
    '#required' => TRUE,
    '#default_value' => $defaults['type'],
  );
  $form['rule'] = array(
    '#type' => 'textarea',
    '#title' => t('Rule'),
    '#description' => t('See the help section above this form.'),
    '#required' => TRUE,
    '#rows' => 3,
    '#default_value' => $defaults['rule'],
  );
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Default Message'),
    '#description' => t('This is the default message sent to the user on validation error. However, this is overwritable for each field. To use placeholders, see the help section above this form.'),
    '#required' => TRUE,
    '#maxlength' => 256,
    '#rows' => 3,
    '#default_value' => $defaults['message'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Validator'),
  );

  return $form;
}

/**
 * The Validate function for the Adding and Editing Validator Form
 */
function validation_api_admin_validators_form_validate($form_id, &$form_state) {
  $values = $form_state['values'];

  // Make sure the type field is a valid input
  switch ($values['type']) {
    case 'regex':
      // Make sure this is valid regex code
    break;
    case 'php':
      // Make sure this is valid PHP code
    break;
    default:
      // Send error when the type field does not use one of the above types
      form_set_error('type', 'Must be a valid type: regex or php');
  }
}

/**
 * The Submit function for the Adding and Editing Validator Form
 */
function validation_api_admin_validators_form_submit($form_id, &$form_state) {
  $values = $form_state['values'];

  // If vavid is present, then update it. Else, insert a new record
  if (isset($values['vavid'])) {
    // Run SQL query to update the vavid. Send message on success. Else, send error.
    if (db_query('UPDATE {validation_api_validators} SET name = "%s", type = "%s", rule = "%s", message = "%s" WHERE vavid = %d', array($values['name'], $values['type'], $values['rule'], $values['message'], $values['vavid']))) {
      drupal_set_message(t('%name has been updated', array('%name' => $values['name'])));
    }
    else {
      drupal_set_message(t('%name could not be updated', array('%name' => $values['name'])), 'error');
    }
  }
  else {
    // Run SQL query to insert a new validator. Send message on success. Else, send error.
    if (db_query('INSERT INTO {validation_api_validators} (name, type, rule, message) VALUES ("%s", "%s", "%s", "%s")', array($values['name'], $values['type'], $values['rule'], $values['message']))) {
      drupal_set_message(t('New Validator Added'));
    }
    else {
      drupal_set_message(t('Cound not add validator'), 'error');
    }
  }
}

/**
 * The form for Deleting Validators
 */
function validation_api_admin_validators_delete($form_state, $vavid) {
  if (!$obj = db_fetch_object(db_query('SELECT name FROM {validation_api_validators} WHERE vavid = %d', $vavid))) {
    drupal_not_found();
    exit;
  }
  drupal_set_breadcrumb(array_merge(drupal_get_breadcrumb(), array(l(t('Validators'), 'admin/build/validation_api/validators'))));

  $form['#redirect'] = 'admin/build/validation_api/validators';
  $form['vavid'] = array(
    '#type' => 'value',
    '#value' => $vavid,
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $obj->name,
  );

  return confirm_form($form, t('Are you sure you want to delete the validator %name?', array('%name' => $obj->name)), 'admin/build/validation_api/validators', '', t('Delete'));
}

/**
 * The Submit function for the Validator Delete form
 */
function validation_api_admin_validators_delete_submit($form_id, &$form_state) {
  $values = $form_state['values'];

  if (db_query('DELETE FROM {validation_api_validators} WHERE vavid = %d', $values['vavid'])) {
    drupal_set_message(t('%name was deleted', array('%name' => $values['name'])));
  }
  else {
    drupal_set_message(t('%name could not be deleted', array('%name' => $values['name'])));
  }
}

/**
 * Lists all fields for the Admin Page
 */
function validation_api_admin_fields() {
  $result = db_query('SELECT * FROM {validation_api_fields} ORDER BY form_id, form_field');
  $validators = _validation_api_validators();

  $header = array(t('Validator'), t('Argument'), t('Ops'));
  while ($obj = db_fetch_object($result)) {
    $forms[$obj->form_id][$obj->form_field][] = $obj;
  }

  $output = '';
  if (isset($forms) && is_array($forms)) {
    foreach ($forms as $form_id => $fields) {
      foreach ($fields as $field_name => $field) {
        $rows = array();
        foreach ($field as $validator) {
          $rows[] = array($validators[$validator->vavid]->name, $validator->arg, l(t('edit'), 'admin/build/validation_api/fields/edit/'. $validator->vafid) .' '. l(t('delete'), 'admin/build/validation_api/fields/delete/'. $validator->vafid));
        }
        $output .= theme('table', $header, $rows, array(), $form_id .' - '. $field_name);
      }
    }
  }
  else {
    $output .= t('There are no fields being validated. !link.', array('!link' => l(t('Add a field'), 'admin/build/validation_api/fields/add')));
  }

  return $output;
}

/**
 * The form for Adding or Editing Fields
 */
function validation_api_admin_fields_form($form_state, $vafid = NULL) {
  $validators = _validation_api_validators();

  // Any validators in the database
  if (count($validators) == 0) {
    $form['validators'] = array(
      '#type' => 'item',
      '#value' => t('There are no validators in the database. !link.', array('!link' => l(t('Add a validator'), 'admin/build/validation_api/validators/add'))),
    );
  }

  // Add Field Step 1
  elseif ((!isset($form_state['rebuild']) || $form_state['rebuild'] !== TRUE) && is_null($vafid)) {
    // Set defaults
    $defaults['form_id'] = isset($_GET['form_id']) ? $_GET['form_id'] : '';
    $defaults['form_field'] = isset($_GET['form_field']) ? $_GET['form_field'] : '';
    $defaults['validator'] = isset($_GET['validator']) ? $_GET['validator'] : '';

    $form['field_form_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Form ID'),
      '#description' => t('The ID of the field\'s form (e.g. comment_form). See the help section above this form.'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#default_value' => $defaults['form_id'],
    );
    $form['form_field'] = array(
      '#type' => 'textfield',
      '#title' => t('Field Name'),
      '#description' => t('The name of the field you want to be validated. See the help section above this form.'),
      '#required' => TRUE,
      '#maxlength' => 64,
      '#default_value' => $defaults['form_field'],
    );
    foreach ($validators as $key => $validator) {
      $options[$key] = $validator->name;
    }
    $form['validator'] = array(
      '#type' => 'select',
      '#title' => t('Validator'),
      '#description' => t('The Validator to test on the field.'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $defaults['validator'],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Field'),
    );
  }
  else {
    $form_state['rebuild'] = FALSE;

    // Add Field Step 2 Defaults
    if (is_null($vafid)) {
      $form['#redirect'] = 'admin/build/validation_api/fields/add';
      $defaults = $form_state['values'];
      $defaults['arg'] = $defaults['allow_empty'] = '';
      $defaults['message'] = $validators[$defaults['validator']]->message;

      $form['step_back'] = array(
        '#type' => 'item',
        '#value' => l(t('Go Back to the previous step'), 'admin/build/validation_api/fields/add', array('query' => array('form_id' => $form_state['storage']['field_form_id'], 'form_field' => $form_state['storage']['form_field'], 'validator' => $form_state['storage']['validator']))),
      );
    }
    // Edit Field Defaults
    else {
      if ($array = db_fetch_array(db_query('SELECT * FROM {validation_api_fields} WHERE vafid = %d', $vafid))) {
        $defaults = $array;
        $defaults['validator'] = $defaults['vavid'];
        $defaults['field_form_id'] = $defaults['form_id'];

        $form['#redirect'] = 'admin/build/validation_api/fields';
        $form['vafid'] = array(
          '#type' => 'hidden',
          '#value' => $vafid,
        );
      }
      else {
        drupal_not_found();
        exit;
      }
    }

    $form['field_form_id'] = array(
      '#type' => 'item',
      '#title' => t('Form ID'),
      '#value' => $defaults['field_form_id'],
    );
    $form['form_field'] = array(
      '#type' => 'item',
      '#title' => t('Field Name'),
      '#value' => $defaults['form_field'],
    );
    $form['validator'] = array(
      '#type' => 'item',
      '#title' => t('Validator'),
      '#value' => $validators[$defaults['validator']]->name,
    );

    $form['arg'] = array(
      '#type' => 'textfield',
      '#title' => t('Argument'),
      '#description' => t('See the help section above this form.'),
      '#maxlength' => 32,
      '#default_value' => $defaults['arg'],
    );
    $form['allow_empty'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow this field to be empty?'),
      '#description' => t('The validator will not be run if the field is empty. If the field is marked as required, then this option will not affect anything as it will be required either way.'),
      '#default_value' => $defaults['allow_empty'],
    );
    $form['message'] = array(
      '#type' => 'textfield',
      '#title' => t('Message'),
      '#description' => t('If this validator throws an error, then this will be the message returned.'),
      '#maxlength' => 256,
      '#default_value' => $defaults['message'],
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save Field'),
    );
  }

  return $form;
}

/**
 * The Validate function for the Adding and Editing Field Form
 */
function validation_api_admin_fields_form_validate($form_id, &$form_state) {

}

/**
 * The Submit function for the Adding and Editing Field Form
 */
function validation_api_admin_fields_form_submit($form_id, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['arg']) && isset($values['allow_empty']) && isset($values['message'])) {
    // If vafid is present, then update it. Else, insert a new record
    if (isset($values['vafid'])) {
      // Run SQL query to update the vavid. Send message on success. Else, send error.
      if (db_query('UPDATE {validation_api_fields} SET arg = "%s", allow_empty = %d, message = "%s" WHERE vafid = %d', array($values['arg'], $values['allow_empty'], $values['message'], $values['vafid']))) {
        drupal_set_message(t('The field has been updated.'));
      }
      else {
        drupal_set_message(t('The field could not be updated.'), 'error');
      }
    }
    else {
      // Get values that were stored during the multi-step form.
      $values['form_id'] = $form_state['storage']['field_form_id'];
      $values['form_field'] = $form_state['storage']['form_field'];
      $values['vavid'] = $form_state['storage']['validator'];
      // Run SQL query to insert a new validator. Send message on success. Else, send error.
      if (db_query('INSERT INTO {validation_api_fields} (form_id, form_field, vavid, arg, allow_empty, message) VALUES ("%s", "%s", %d, "%s", %d, "%s")', array($values['form_id'], $values['form_field'], $values['vavid'], $values['arg'], $values['allow_empty'], $values['message']))) {
        drupal_set_message(t('New field added.'));
      }
      else {
        drupal_set_message(t('Could not add field.'), 'error');
      }
    }
  }
  else {
    $form_state['storage'] = $form_state['values'];
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * The form for Deleting Fields
 */
function validation_api_admin_fields_delete($form_state, $vafid) {
  if (!$obj = db_fetch_object(db_query('SELECT f.form_field AS form_field, v.name AS name FROM {validation_api_fields} AS f JOIN {validation_api_validators} AS v ON f.vavid = v.vavid WHERE vafid = %d', $vafid))) {
    drupal_not_found();
    exit;
  }
  drupal_set_breadcrumb(array_merge(drupal_get_breadcrumb(), array(l(t('Validators'), 'admin/build/validation_api/fields'))));

  $form['#redirect'] = 'admin/build/validation_api/fields';
  $form['vafid'] = array(
    '#type' => 'value',
    '#value' => $vafid,
  );
  $form['form_field'] = array(
    '#type' => 'value',
    '#value' => $obj->form_field,
  );

  return confirm_form($form, t('Are you sure you want to delete the validator %validator from being used on the field %field?', array('%validator' => $obj->name, '%field' => $obj->form_field)), 'admin/build/validation_api/fields', '', t('Delete'));
}

/**
 * The Submit function for the Field Delete form
 */
function validation_api_admin_fields_delete_submit($form_id, &$form_state) {
  $values = $form_state['values'];

  if (db_query('DELETE FROM {validation_api_fields} WHERE vafid = %d', $values['vafid'])) {
    drupal_set_message(t('%field was deleted', array('%field' => $values['form_field'])));
  }
  else {
    drupal_set_message(t('%field could not be deleted', array('%field' => $values['form_field'])));
  }
}

/**
 * The Export form for validators. After submit, this will create a textarea for the export code.
 */
function validation_api_admin_export_form($form_state) {
  $validators = _validation_api_validators();
  if (isset($validators) && is_array($validators) && count($validators) > 0) {
    if ($form_state['submitted']) {
      $form['export_code'] = array(
        '#type' => 'textarea',
        '#title' => t('Export Code'),
        '#default_value' => $form_state['storage'],
        '#rows' => 10,
      );
    }

    foreach ($validators as $key => $validator) {
      $options[$key] = $validator->name;
    }

    $form['validators'] = array(
      '#title' => t('Validators to Export'),
      '#type' => 'checkboxes',
      '#options' => $options,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Export',
    );
  }
  else {
    $form['validators'] = array(
      '#type' => 'item',
      '#value' => t('There are no validators in the database. !link.', array('!link' => l(t('Add a validator'), 'admin/build/validation_api/validators/add'))),
    );
  }

  return $form;
}

/**
 * The Submit function for the Validator Export form.
 */
function validation_api_admin_export_form_submit($form_id, &$form_state) {
  $form_state['storage'] = validation_api_export($form_state['values']['validators']);
}

/**
 * The Import form for validators.
 */
function validation_api_admin_import_form() {
  $validators = _validation_api_validators_from_hook();

  if (isset($validators) && is_array($validators) && count($validators) > 0) {
    foreach ($validators as $key => $validator) {
      $options[$key] = t('%name from %key', array('%name' => $validator->name, '%key' => $key));
    }

    $form['validators'] = array(
      '#title' => t('Import Validators from Modules'),
      '#type' => 'checkboxes',
      '#options' => $options,
    );
  }
  else {
    $form['validators'] = array(
      '#title' => t('Import Validators from Modules'),
      '#type' => 'item',
      '#value' => t('There are no validators to import from modules.'),
    );
  }

  $form['code'] = array(
    '#type' => 'textarea',
    '#title' => t('Import Code'),
    '#description' => t('The code for validators.'),
    '#rows' => 15,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Import',
  );

  return $form;
}

/**
 * The Submit function for the Validator Import form.
 */
function validation_api_admin_import_form_submit($form_id, &$form_state) {
  $validators = eval($form_state['values']['code']);
  if (isset($validators) && is_array($validators)) {
    foreach ($validators as $key => $validator) {
      if (isset($validator->module)) {
        unset($validators[$key]->module);
      }
      if (isset($validator->delta)) {
        unset($validators[$key]->delta);
      }
    }
  }
  if (isset($form_state['values']['validators']) && is_array($form_state['values']['validators'])) {
    foreach ($form_state['values']['validators'] as $key) {
      $array = explode('-', $key);
      $module = array_shift($array);
      $delta = (int) array_shift($array);
      if (!isset($hook_validators[$module])) {
        $hook_validators[$module] = module_invoke($module, HOOK_ADD_VALIDATOR);
      }
      $validators[$key] = $hook_validators[$module][$delta];
      $validators[$key]->module = $module;
      $validators[$key]->delta = $delta;
    }
  }
  if (isset($validators) && is_array($validators)) {
    _validation_api_import($validators);
  }
  else {
    drupal_set_message(t('Validator(s) were not returned in a proper format. Needs to be an array.'), 'error');
  }
}

/**
 * The Clone form for validators.
 */
function validation_api_admin_clone_form() {
  $validators = _validation_api_validators();

  if (isset($validators) && is_array($validators) && count($validators) > 0) {
    foreach ($validators as $key => $validator) {
      $options[$key] = $validator->name;
    }

    $form['validators'] = array(
      '#title' => t('Validators to Clone'),
      '#type' => 'checkboxes',
      '#options' => $options,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Export',
    );
  }
  else {
    $form['validators'] = array(
      '#type' => 'item',
      '#value' => t('There are no validators in the database. !link.', array('!link' => l(t('Add a validator'), 'admin/build/validation_api/validators/add'))),
    );
  }

  return $form;
}

/**
 * The Submit function for the Validator Clone form.
 */
function validation_api_admin_clone_form_submit($form_id, &$form_state) {
  _validation_api_clone($form_state['values']['validators']);
}

/**
 * The form for Updating Validators
 */
function validation_api_admin_update_form($form_state, $vavid) {
  if (!$obj = db_fetch_object(db_query('SELECT * FROM {validation_api_validators} WHERE vavid = %d', $vavid))) {
    drupal_not_found();
    exit;
  }
  drupal_set_breadcrumb(array_merge(drupal_get_breadcrumb(), array(l(t('Validators'), 'admin/build/validation_api/validators'))));

  $form['#redirect'] = 'admin/build/validation_api/validators';
  $form['vavid'] = array(
    '#type' => 'value',
    '#value' => $vavid,
  );
  $form['name'] = array(
    '#type' => 'value',
    '#value' => $obj->name,
  );
  $form['module'] = array(
    '#type' => 'value',
    '#value' => $obj->module,
  );
  $form['delta'] = array(
    '#type' => 'value',
    '#value' => $obj->delta,
  );
  $fields = array('type', 'rule', 'message');
  $form['current_code'] = array(
    '#type' => 'textarea',
    '#title' => t('Current code'),
    '#default_value' => _validation_api_export(array($obj), $fields),
  );
  $validators = module_invoke($obj->module, HOOK_ADD_VALIDATOR);
  $validator = $validators[$obj->delta];
  $hook_code_value .= '$validators[0]->type = \''. $validator->type ."';\n";
  $hook_code_value .= '$validators[0]->rule = \''. $validator->rule ."';\n";
  $hook_code_value .= '$validators[0]->message = \''. $validator->message ."';\n";
  $hook_code_value .= 'return $validators;';
  $form['hook_code'] = array(
    '#type' => 'textarea',
    '#title' => t('New code'),
    '#default_value' => $hook_code_value,
  );

  return confirm_form($form, t('Are you sure you want to update the validator %name?', array('%name' => $obj->form_field)), 'admin/build/validation_api/validators', '', t('Update'));
}

/**
 * The Submit function for the Validator Update form
 */
function validation_api_admin_update_form_submit($form_id, &$form_state) {
  $values = $form_state['values'];
  $hook_validators = module_invoke($values['module'], HOOK_ADD_VALIDATOR);
  $validator = $hook_validators[$values['delta']];

  if (db_query('UPDATE {validation_api_validators} SET type = "%s", rule = "%s", message = "%s" WHERE vavid = %d', array($validator->type, $validator->rule, $validator->message, $values['vavid']))) {
    drupal_set_message(t('%name was updated.', array('%name' => $values['name'])));
  }
  else {
    drupal_set_message(t('%name could not be updated.', array('%name' => $values['name'])));
  }
}

/**
 * Export function that returns the export code using Validator IDs.
 *
 * @param $vavids
 *   An array of validator IDs.
 *
 * @return
 *   Returns export code that can be used to import the validator in other applications.
 */
function validation_api_export($vavids) {
  $wheres = array();
  for ($i = 0; $i < count($vavids); $i++) {
    $wheres[] = 'vavid = %d';
  }
  $result = db_query('SELECT name, type, rule, message FROM {validation_api_validators} WHERE '. implode(' OR ', $wheres), $vavids);

  $validators = array();
  while ($validator = db_fetch_object($result)) {
    $validators[] = $validator;
  }

  return _validation_api_export($validators);
}

/**
 * Export helper function that creates and returns the export code from validator objects.
 *
 * @param $validators
 *   An array of validator objects.
 * @param $fields
 *   An array of fields that are included in the export code.
 *
 * @return
 *   Returns export code that can be used to import the validator in other applications.
 */
function _validation_api_export($validators, $fields = array('name', 'type', 'rule', 'message')) {
  $output = '';
  $i = 0;
  foreach ($validators as $validator) {
    foreach ($fields as $field) {
      $output .= '$validators['. $i .']->'. $field .' = \''. $validator->$field ."';\n";
    }
    $i++;
  }
  $output .= 'return $validators;';

  return $output;
}

/**
 * Helper function that inserts validators into the database from an array of validators.
 *
 * @param $validators
 *   An array of validators.
 *
 * @return
 *   Nothing is returned.
 */
function _validation_api_import($validators) {
  $db_validators = _validation_api_validators();
  $validator_names = array();
  foreach ($db_validators as $validator) {
    $validator_names[] = $validator->name;
  }
  unset($db_validators);
  $values = array();
  $args = array();
  foreach ($validators as $validator) {
    if (isset($validator->name) && isset($validator->type) && isset($validator->rule) && isset($validator->message)) {
      $str = '("%s", "%s", "%s", "%s"';
      if (in_array($validator->name, $validator_names)) {
        $i = 1;
        $open_validator = FALSE;
        do {
          if (!in_array($validator->name . $i, $validator_names)) {
            $validator->name .= $i;
            $open_validator = TRUE;
          }
          else {
            $i++;
          }
        } while (!$open_validator);
      }
      array_push($args, $validator->name, $validator->type, $validator->rule, $validator->message);
      if (isset($validator->module) && isset($validator->delta)) {
        $str .= ', "%s", %d)';
        array_push($args, $validator->module, $validator->delta);
      }
      else {
        $str .= ', "", 0)';
      }
      $values[] = $str;
    }
  }
  if (count($values) > 0) {
    if (db_query('INSERT INTO {validation_api_validators} (name, type, rule, message, module, delta) VALUES '. implode(', ', $values), $args)) {
      drupal_set_message(t('Successfully created %count validator(s).', array('%count' => count($validators))));
    }
    else {
      drupal_set_message(t('Unsuccessful at creating validator(s).'), 'error');
    }
  }
  else {
    drupal_set_message(t('There were not any valid validators to be created.'), 'error');
  }
}

/**
 * Helper function for cloning validators. It runs export, then runs import on the returned export code.
 *
 * @param $vavids
 *   An array of validator IDs.
 *
 * @return
 *   Nothing is returned.
 */
function _validation_api_clone($vavids) {
  $validators = eval(validation_api_export($vavids));
  _validation_api_import($validators);
}
