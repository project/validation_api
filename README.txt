Validation API Module
Author: Craig Jackson <tapocol [at] gmail [dot] com>
This module is apart of the 2008 Google Summer of Code.
Validation API allows to be able to add validation rules to any field on your site. However, the latest versions are limited to text-based core fields.
Currently, the JavaScript portion is not tied in, and it will be getting switched over to jQuery.

How to Use:
1.) Install like any other Drupal module.
    Place validation_api in your modules folder (recommend: /sites/all/modules/).
    Goto admin/build/modules, check enabled on Validation API, and click submit.
2.) Creating validators.
    There are three options:
      Admin Form (admin/build/validation_api/validators/add)
      Importing validators (admin/build/validation_api/validators/import)
      Through 'add_validators' hook to upload the validators from the hook, goto the import page
        (see 'validation_api_add_validators' function for more info on the hook)
    You can create php or regex types.
    The rule for php types uses $value for the value entered and $arg, if there is an argument needed for each field.
    The rule for regex types uses %arg, if there is an argument needed for each field.
3.) After adding validators, then create relationships with the validator and fields.
    Add field (admin/build/validation_api/fields/add).
    If you do not know the form id and/or field name of the field you want to validate,
      you can goto the form on the site, and click the 'Add validator to this field' link.
      **** WARNING: There is problems with CCK fields and types that are not text-oriented fields.
      Check out the Validator + Field Example below to see what the argument field is used for.
4.) You should be able to go to that form and validate with your new condition.

Validator Example:
Fill out the form at admin/build/validation_api/validators/add
Name: email
Type: php
Rule: return valid_email_address($value);
Message: %field needs to be a valid email address.

Validator + Field Example:
Make sure you have a node created
Import the hook that comes with this module (length) at the import page.
Then, goto the fields section, and add this:
Form ID: comment_form
Field Name: comment
Validator: length
Argument: 10

This will validate comment_form's comment field on submit, and make sure it is at least 10 characters.
Go test it to make sure, try to add a comment to a node. You can use preview to test so you don't submit comments.