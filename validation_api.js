
$(document).ready(function() {
  $('form').submit(function() {
    var form = $(this);
    var success = false;
    $('#'+ form.attr('id') +' .error').removeClass('error');
    var args = form.serializeArray();
    var path = form.attr('action').substring(Drupal.settings.basePath.length);
    var response = $.ajax({
      url: Drupal.settings.basePath + 'validation_api?action=' + path,
      async: false,
      cache: false,
      data: args,
      dataType: 'json',
      type: 'POST',
      success: function(data) {
        var thisform = form;
        if (data.success) {
          alert("Success\n"+ data.test);
          success = true;
        }
        else {
          var errors = data.errors;
          var message = '';
          for (var i = 0; i < errors.elements.length; i++) {
            $('#edit-' + errors.elements[i].replace(/_/, '-')).addClass('error');
            message += errors.messages[i] + "\n";
          }
          alert(message + data.test);
          return false;
        }
      },
      error: function(data) {
        alert("Could not connect properly.\n"+ data.responseText);
      }
    });
    return success;
  });
});
